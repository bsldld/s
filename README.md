A summary of the project idea is available [here](https://docs.google.com/presentation/d/1GPen8FWmTr8QF1HQ4hkLLIb8QmUEODwIp0PfHUy2ENY/edit?usp=sharing).

#### Introduction

This is a non-profit project to develop a free and open source software platform to help make education affordable to everyone but without using student loans. The project will also reduce government's student loan burden and increase income of education institutions and its teaching and non-teaching staff.

This platform will also help education policymakers in informed decision making due to on-time availability of reliable and correlated data on education and the economy.

The software platform will be free for anyone to deploy(install) and use. Education institutions and government departments will themselves be able to deploy the software platform to connect to the network or they could directly use the platform hosted by other organisations. This is a decentralised, but federated, platform that no single entity owns or controls, but everyone will be able to freely connect and use.

The goal of the project is to:
* make education affordable without the use of student loans.
* increase income of teaching and non-teaching staff and education institutions.
* help education policymakers in decision making by providing timely and accurate data.
* reduce student loan burden on government and hence tax payers.
* facilitate collaboration between employers and education institutions from an early stage in the student education process.
* help institutions and businesses identify and nurture talent at global scale.
* enable education institutions and policymakers to rapidly adapt to feedback from society and businesses.

#### How?

The idea is simple. A student gets education for free, but when that student starts generating income then a small percentage from that income is auto-deducted and distributed to everyone who was involved in that student's education.

This project is developing a software platform to facilitate this idea. It will act as a glue between the education institutions, the students, the government education and revenue/treasury departments and the businesses. The platform will maintain links between the government departments, the education institutions, its staff, the students and their employers.

As it presently happens in many countries with the auto-deduction of student loan repayment directly from student salaries, this platform too will work in a similar manner by auto-deducting a small percentage from student's income. But instead of deducting the amount because of loan, the deduction will happen because the student has received education. This eliminates the need for loans. And instead of the government giving loans to students and then students paying the fees using that, and then afterwards repaying these loans back to government, with this platform the students pay from their income directly to the education institutions and its staff for the education they have received. The government, if required, will then immediately pay(via grants and loans) directly to the education institution to top-up any shortfall in their staff's salaries after they receive payments from students(For details of how this works, please refer to Appendix of the detailed document [here](README_detailed.md)). This system can be easily automated and is achievable using a software platform.

Following diagrams show the difference between the flow of funds in the present student loan system and the proposed system.

```
Flow of funds in the present system:
   +======================== C. Loan repayment =================================+
   |                                                                            |
   |  +====================== B. Grants/Loans ===============================+  |
   |  |                                                                      |  |
   |  |  +==== A. Request for grants/loans(research, expenses etc) ======+   |  |
   |  |  |                                                               |   |  |
   |  |  |  +====== 5. Loan Repayment ======+                            |   |  |
   |  |  |  |                               |                            |   |  |
   v  |  v  v                               |                            |   v  |
 +============+                        +=========+                +=======================+                  +=======+
 | Government | == 1. Student Loan ==> | Student | == 2. Fees ==> | Education Institution | == 3. Salary ==> | Staff |
 +============+                        +=========+                +=======================+                  +=======+
                                            ^
                                            |
                                       4. Income
                                            |
                                     +===============+
                                     | Income Source |
                                     +===============+

1. Students receives education loans.
   - Interest on the loan starts accummulating immediately.
2. Students pay fees with these loans.
3. Education Institutions pay salaries to its staff.
4. Students start getting income.
5. Students pay back loan + interest.

Education institutions get separately funded by the government for other expenses and research.
A. Education institutions apply for grants and loans to the government for research, expenses etc.
B. Government provides grants and loans to education institutions.
C. Education institutions repay their government loans.
```

```
Flow of funds in the proposed system:
    +=========== 6. Loan repayment ====================+
    |                                                  |
    |   +=====================================+        |
    |   |                                     |        |
    |   |     1. Request for grants/loans(research, expenses, top-up salaries)
    |   |                                     |        |
    v   v                                     |        |
 +============+                        +=======================+                                       +=======+
 | Government | == 2. Grants/Loans ==> | Education Institution | == 3. Salary Top-up(if required)* ==> | Staff |
 +============+                        +=======================+                                       +=======+
                                               ^                                                           ^
                                               |                                                           |
                                         5b. Payment                                                       |
                                               |                                                           |
         +===============+                +=========+                                                      |
         | Income Source |-- 4. Income -->| Student |====== 5a. Payment ===================================+
         +===============+                +=========+

* For the concept of salary top-up see Appendix of the detailed document linked above in this document.

1. Education institutions apply for grants and loans to the government for research, expenses and staff salary-topups.
2. Government provides grants and loans to education institutions.
3. Education institutions pay salaries to staff.
4. Students start getting income.
5. Students make payment to education institutions and its staff.
6. Education institutions repay their government loans.
```

Education institutions register their students and staff on this platform. This allows the platform to make connections between the staffs and the students. The government's education and revenue/treasury departments too connect to the platform to help facilitate fund transfers to education institutions and or its staff, and for fund transfer from student to the education institutions and its staff. When a student gets into employment, the employer using this platform deducts the specified percentage from the student's income and that amount is then automatically credited by this platform to the accounts of education institutions and its staff. Any amount that is owed by the education institutions to the government is also paid to the government departments using this platform.

#### Benefits of the proposed platform

###### For the government
- Government will be able to reduce the student loan burden.
- Policymakers will be able to get accurate and timely data on correlation between education policies and economy.
- Government will be able to deploy more funds for necessary but less funded courses such as Arts. One of the aims of this platform is to also promote blending of Arts subject into STEM subjects to form STEAM subjects.
- Government will be able to deploy more funds to support differently-abled students. This platform too will contribute funds to support education of differently-abled students.

###### For the education institutions and its staff
- Education institutions will be able to generate additional revenue stream.
- Income of staff will increase.
- Education institutions will be able to compete with the best paying industries to attract the best talent.
- Education institutions will be able to adapt to immediate signals from the society regarding education.
- More funds will be available for other essential but less funded courses. It will be possible to blend Arts subjects with STEM subjects to form STEAM subjects.

###### For students
- Education will be affordable without the need for student loans.
- Students will be able to try various different courses due to lack of loan requirements.
- There will be no loan burden on students, and hence no fear of interest accumulation during unemployment.
- There is no possibility of losing credit ratings as there is no loan.
- This is a far more fairer system for payment than the present loan repayment system.
- Instead of having high repayment percentage that is pegged to inflation(as is the case with present loan system), the per month student contribution as proposed by this project will be less as it depends only on the income.
- The payment schedule will also be distributed over longer period to reduce the monthly payments.

###### For the society
- The possibility of tax rises due to student loan defaults is eliminated. In present system, in addition to paying student loan, an employed person will also have to pay increased taxes to cover the cost of defaulted student loans.
- Elimination of student loans will encourage continuous learning so that citizens will be able to upgrade their skills and be in the workforce.
- Education as a whole will be well funded and attract best talent.
- Citizens with education loan defer buying homes or taking other types of loans such as business loans which indirectly affects the growth of the economy.

###### For businesses
- Connection to education institutions from the early stages of student education will be possible.
- Early feedback to the education sector can be provided using this platform.
- Access to the right talent for the industry will be available due to close collaboration between education sector and businesses.
- Businesses, if they want, will be able to easily fund education.


Please provide feedback to: bsldld@socialworker.net


A detailed description of the project idea, the rationale for the project and the challenges that will be faced by this project is available [here](README_detailed.md).
