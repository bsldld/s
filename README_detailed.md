A summary of the project idea is available [here](https://docs.google.com/presentation/d/1GPen8FWmTr8QF1HQ4hkLLIb8QmUEODwIp0PfHUy2ENY/edit?usp=sharing).

#### Introduction
Governments all over the world aspire to provide basic services, such as education, _free_ of cost to its citizens. This is a noble and just cause that should be implemented. But a _free_ service actually creates a massive debt burden on the government. This will have to be paid back some day by its citizens in the form of increased taxes. If the debt is written-off then the fallout of that is inflation(i.e. rise in cost of everyday items such as food, housing and medicine). The money spent on education has to be somehow recouped to redeploy it again for education. Governments all over the world have struggled with this conundrum for decades. This was aptly presented in a popular TV series[^1] in the US.

There are lots of young Einsteins, Newtons and Ramanujans in this world who do not get the right exposure to education in a timely manner due to lack of funds. Similarly there are lots of dedicated teachers who are fervorously helping their students to excel but are handicapped by circumstances e.g. poverty and or lack of resources. This project also aims to mitigate that situation.

This is a non-profit project to develop a free and open source software platform to help eliminate student loans, reduce government debt burden and increase income of education institutions and its teaching and non-teaching staff. The platform will also help education policymakers in informed decision making due to on-time availability of reliable and correlated data on education and employment. This will be a decentralised, distributed and federated software platform that government organisations, education institutions and other interested bodies can freely deploy and use.

The goal of the project is to help facilitate:
* elimination of student loans.
* lowering the education entry barrier for students from a monetary perspective.
* increase in income of teaching and non-teaching staff and education institutions.
* education policymakers in decision making by providing timely and accurate data.
* collaboration between employers and education institutions from an early stage in the student education process.
* institutions and businesses identify and nurture talent at global scale.
* education institutions and policymakers to rapidly adapt to feedback from society and businesses.

Feedback is most welcome, email: bsldld@socialworker.net

Please do participate in the project in any way possible as it is open to everyone, educationists, policymakers, software engineers, parents, students, teaching and non-teaching staff, employers or anyone who is interested in a better education system.

The following sections discuss student loan problems faced by two countries, the USA and the UK. But this problem of massive government education debt and students getting in debt from early on in their life is endemic in all countries that provide loans to students for education.

#### Current Solutions
There are three widely used options for providing "free" education:
* Government loans and grants
  - Government provides grants to education institutions and teaching staff, and a combination of loans and grants to students. This is supported by a combination of taxes and interest payments on loans.
  - Students pay back loans once they are in employment(in case of the UK and US) or after a grace period after completing education(in case of US). The interest on loan starts accumulating immediately after the loan is disbursed.
* Privatisation of student loans
  - In the UK the government is slowly moving towards privatisation of loans by selling its loan books to private investors[^2].
  - In the US some education institutions are already collaborating with private investors to provide loans to students under the Income Share Agreement(ISA) scheme.
  - Students start repaying the loan once they are in employment.
* Government funds free education by raising taxes on all citizens.

#### Problems with current solutions
* Government loans and grants
  - Students are left with massive debt burden and future financial challenges(e.g. a student may not get a mortgage) due to loan defaults and bad credit ratings even before they are in employment.
  - Students will be hit hard twice in the future, first with the loan they owe and next with the tax increase because most of the student loans are never paid back.
  - Students who enter low paying professions such as teaching, public sector jobs or social work have higher loan repayment, as a ratio of their earnings[^3].
  - There is no way to track loans and grants with tangible monetary outcomes. Success is tracked with intangible outcomes such as pass rate, grades and employment figures.
  - Governments do not get reliable and timely data on funds spent on education to the actual outcomes; there is no real correlation between money spent and the return on investment per student.
  - Education policymakers do not get reliable and timely data to map policies to actual outcomes for informed decision making.
* Privatisation of student loan
  - Giving control of student loans to private investors will put students at greater disadvantage than in the present situation where the loan provider is the government.
* Increasing taxes on everyone to fund free education penalises low-income earners and people who have not benefited from education.
* Stagnation of teacher's pay
  - Teachers/Professors/Lecturers are the architects of future generations and society. Yet they are not paid the best salaries compared to other high-paying industries/professions.
  - Hence it is difficult to attract and retain talent in the education sector.
* Employers have difficulty finding talent as per their requirements as there is no synchronisation between industries and education institutions.
* Education institutions take time to adapt to changing industry and society demands as automatic and immediate feedback signals are almost non-existent.
* Decreasing tuition/fees affects education institutions and its staff's income.
* The UK and US government have no formal way to recover loans from students who have moved overseas.
* Due to availability of loans the cost of education has risen[^4].
* Students cannot switch courses as it is expensive to do so.
* Citizens stay away from continuous learning as education is expensive.

###### Problems faced by the US
* U.S. expenditures for public and private education, from prekindergarten through graduate school (excluding postsecondary schools not awarding associate’s or higher degrees), were an estimated $1.4 trillion for 2017–18[^5].
* The total amount of outstanding student loans in the US is over $1.5 trillion(2019 figure)[^6] and growing(this is greater than credit card and auto loans[^7]!).
* The estimated cost to taxpayers will be $307 billion[^8] for loans issued over the next decade and that largely excludes the cumulative losses already anticipated on loans issued prior to 2019.
* Average 30% students default on their loans[^9].
* Average debt a student of 2018 owes is from $19,750 to $38,650(varies across State)[^10].
* Student loan defaults are reported to credit bureaus, damaging credit rating of students and affecting their ability to buy a car, a house or to get a credit card[^33].
* Roughly 20% of the decline in home ownership among young adults can be attributed to their increased student loan debts since 2005[^11].
* The rise in cost of college education is also attributed to loan availability to students[^4].
* Some education institutions have started supporting the Income Share Agreement[^12].
* 8% teachers left the profession in a year in 2012-13[^13]<sup>,</sup>[^14].
* Public school enrollment was 3 percent higher in 2018 than in 2008, while the number of public school teachers was 1 percent lower[^15].
* The number of public school pupils per teacher increased from 15.3 in 2008 to 16.0 in 2018[^16].
* The average salary for public school teachers in 2017–18 was $60,483. In constant (i.e., inflation-adjusted) dollars, the average teacher salary was 1 percent lower in 2017–18 than in 1990–91[^16].

###### Problems faced by the UK
* The total amount of outstanding student loan is £121 billion(as of 2019; by 2050 it will reach around £450 billion)[^17].
* Only about 30% students are expected to pay back the loan[^17].
* Average debt a student has after completing education is £36,000[^17].
* The estimated cost to taxpayers just for a single higher education student is at least £7K per year[^18].
* The UK government spends roughly about £25K per student per year in England at all the stages of education[^19].
* England spends roughly £73,000 per student education from early years to completion of education[^20].
* The UK government has already piloted a policy to sell student loans on its books to private investors[^21]<sup>,</sup>[^2].
* Of the teachers who qualified in 2013, over 32% left the service by 2018[^23]<sup>,</sup>[^24]<sup>,</sup>[^25]<sup>,</sup>[^26].

This situation is unsustainable. There is a need for an alternate way to fund student education to reduce the debt burden on governments, eliminate debt burden on students and at the same time increase teaching, non-teaching staff and education institution income.

#### Proposed Solution
Students getting into debt and on the other hand staff at education institutions not getting paid competitive salaries is not a position any country would like to sustain. Till date governments, and hence taxpayers, have been piling up massive debts to mitigate this situation. The stressed education funding and the fallout of this on society needs to be handled in a way that benefits both students and the education institution and its staff, but at the same time does not put pressure on the taxpayer. Privatisation of student loans is not a solution as it will put students at a greater disadvantage.

The aim of this project is to connect governments, students, teaching staff, non-teaching staff, education institutions and employers on a single open source federated platform. This will help in developing a very simple idea:

_A student does not pay fees upfront for education. But when that student starts earning income, a small percentage from that pre-tax income is auto-deducted and distributed each month to everyone who was involved in that student's academic education. The education institutions and its teaching and non-teaching staff earn income cumulatively every year. Any shortfall in the income of education institution and its staff is covered by the government, if required._

In the UK, the recent Post-18 Review panel/Augar Report[^27] actually recommends reducing student loans and increasing teacher's grants! So the UK government could move in that direction. Also, universities in the US are experimenting with Income Share Agreement(ISA) where a student gets free education but has to pay back the loan from salary to a private investor who has funded that student's education.

This platform is somewhat similar to ISA. But students do not have to take education loans. And instead of paying to private investors student payments, after they start earning income, are automatically done to educators. Also, the fund provider, if required by education institutions, is the government rather than private investors.

The platform will help maintain connections between students, their employers, teaching and non-teaching staff and education institutions, and use that information to collect and pay the monthly amount from alumni's income to the recipients.

Interestingly this type of system is implemented in the UK but in a different way, where a student is given a loan(for 25-30 years) and the repayment of that loan is deducted automatically from that student's pre-tax income. The platform we are building is also based on automatic deduction of a small percentage amount from a student's pre-tax income not because the student has a loan but because the student has received free education. This avoid students from officially having debt but are still required to pay to support the education system that they benefited from.

This platform, at present, deals with only education/tuition fees and not with maintenance loans/living expenses.

Another important outcome of this platform is to enable governments, policymakers and education institutions to devise education policies for better outcomes for students. With the help of this platform it will be possible for government and policymakers to identify in tangible monetary terms where spending has to be done in education. This will help in better outcomes for students as well as the government and education sector. This will also make additional funds available with the government that can be released for supporting liberal arts education. More funds will also be available to support education of differently-abled students.

It will be easy to transition to this new system of student contribution. Initially the education institutions will be provided with a combination of loans and grants to cover any shortfall in income after receiving payments from students.
Once the education institutions become self-sufficient from student contributions then the need for government funding will reduce.

Following are some of the features of the platform:
* Students will get to study courses of their choice absolutely free.
* An education institution will set the base salary of its employees every year, similar to the present salary system.
* When a student starts earning income a very small percentage(e.g 5% or less) of that pre-tax income will be auto-deducted and distributed to all the education institutions and staff who were involved in that student's education. (In the UK with the current system the income deduction is at least 9% for graduates and at least 15% for postgraduates for 25-30 years with a inflation pegged interest of 5.4%!(for 2020)[^28]. In the US it varies depending on loan type and can be 20% for 10 to 30 years with interest between 4% to 7%(for 2020)[^29])
* If the monthly student payments received by a staff of an education institution is lower than the base salary set by that education institution then the institution(along with the government) will pay the remaining amount to cover the shortfall. For more details see [Appendix](#appendix).
* If a staff's income due to student payments exceeds the base salary set by the education institution, and previously the staff's base salary was topped-up by government and or education institution then the excess payment will be first paid to the government and or education institution.
* If a student becomes unemployed then the payment from that student will stop for the duration of the unemployment.
* The deductions will happen for the lifetime of the student employment or up to the staff's retirement age, whichever is shorter.
* In case of a staff's demise before retirement, the payments received from the students will go to the nominee of that staff every month up to the staff's retirement date; nominees can be parents/guardian, spouse/partner or able and healthy children below the age of 20; age restriction will not apply to children who are differently abled.
* Only student payments will be paid to the nominee and the education institution and government will not top up any shortfall in the base salary amount.
* A student can increase the payment for teaching staff, non-teaching staff, course/program and or education institution.
* An employer can make additional payments to programs/courses and or education institutions.
* A student's payment will be distributed to the recipients as follows: Teaching staff: 75%, Non-teaching staff: 15%, Education Institution: 9.75% and Platform: 0.25%(The platform can work on donations, but a more viable long-term self-sustaining option is a better one. Two TED talks were instrumental in forming this view[^30]<sup>,</sup>[^31]. Suggestions on alternate ways to keep the platform regularly funded are very much welcome).
* While calculating the percentage payment, the duration of teaching/non-teaching staff employment, attendance record, the duration of teaching/non-teaching staff association with the student and the duration of the course enrollment of a student should be considered; percentage for temporary staff should also be considered according to their time contribution.
* The platform will earmark a certain percentage of the funds it receives for teaching staff, non-teaching staff and institutions working with differently-abled students as these students may not be able to get jobs. This will be in addition to government and other institution funds. Some funds will also be used to promote embedding Arts subjects into STEM curriculum to form STEAM(Science, Technology, Engineering, Arts and Maths) education.
* Association between student, teaching staff, non-teaching staff and educational institutions(school, college, university) will be done when a student takes admission and enrolls for a course/subject at an institution and when staff takes a job at the institution.
* Student and employer association will be done when a student takes a job at the employer.
* The association of the platform participants shall ideally start from the early childhood of the student, start of the education institution, start of staff's employment, and from course/program's first introduction.
* The platform will apply, with the help of employer and government, base percentage deduction to the student's/employee's income.
* The student payment system will not affect the pension and insurance contribution for the staff.
* Education institutions/government should publish the maximum percentage payment students will have to make every month once they are in employment.
* Open source Hyperledger, Sovrin(digital credentials) and Stellar(payment) platforms shall be used to build the platform.

If governments are providing maintenance(living expenses) loans to students then that still needs to be paid by students as it is done currently. The platform in the first phase will not support maintenance loans because it will become complicated to support student expenses outside education institutions; in the next phase of development the platform will probably support rent expenses and include landlords on the platform; after that it could be food expenses and grocery stores.

Outline of how different entities within the platform will interact:
* Education Institution registers with the Education Department; the Platform is notified.
* The Education Institution employs Staff; the Education Department is notified, which notifies the Platform.
* Student registers at the Education Institution; Education Department is notified, which notifies the Platform.
* If the Education Institution claims grant and or loan for the Student from the Education Department, the Platform is notified.
* The Education Institution assigns Staff to Students; the Platform is notified. (Education Institution, Staff and Student/Parent should approve the link/association)
* Employer registers with the Tax Department; the Platform is notified.
* Employer hires Student; Tax Department is notified, which notifies the Platform.
* Employer pays salary to Student or self-employed Student files tax-return; Tax Department is notified, which notifies the Platform.
* Employer deducts percentage contribution from Student's salary or Tax department deducts it from self-employed Student's income. The deduction is then forwarded to the Platform which then distributes it to the appropriate recipients.
* If applicable, the Platform forwards any Education Institution loan repayment amount to the Education Department. 
* If the amount distributed to Staff falls below base salary then the Education Department and Education Institution covers the shortfall.

The platform lets everyone who benefits from education pay directly to the educators for their contribution to society. This will help develop and maintain a healthy and self-sustainable education system.

Outline of how entities within the platform as connected:
* One or more Students are associated with one or more Education Institutions
* One or more Education Institutions are associated with one or more Staff
* One or more Students are associated with one or more Staff
* One or more Students are unemployed or employed and associated with one or more Employer
* One or more Education Institutions are associated with Education Department
* One or more Staff are associated with one or more Education Departments(of registered countries)
* One or more Students are associated with one or more Education Departments(of registered countries)
* One or more Employers are associated with Tax Department
* Platform gets Students and associated Employers data from one or more Tax Departments(of registered countries)
* Platform gets Students, Staff and associated Education Institutions data from one or more Education Departments(of registered countries)
* One or more Employers deduct amount from Student's pre-tax salary and sends to Platform or Tax department deducts amount from self-employed Student's pre-tax income and sends to Platform
* Platform distributes money to one or more Staff
* Platform distributes money to one or more Education Institutions
* Platform distributes money to one or more Education Departments(of registered countries)
* The Education Department notifies loan/grant disbursement for Education Institution or Staff to the Platform.

#### Benefits of the platform
* Students will receive free education without taking an education loan. This will reduce the education entry barrier for students from a monetary perspective.
* Providing free education will not decrease but increase income of education institutions and its staff.
* Increase in education institutions and its staff's income will help attract and retain talent in the education sector.
* Free education will open up education to the underprivileged students.
* Increased income of education institutions and its staff will provide much needed resources to the staff.
* The platform will help reduce government education debt.
* Informed decision making will be possible because better and timely insight into the effects of education policies will be available to government and policymakers.
* Due to involvement of employers on this platform it will facilitate collaboration between employers and education institutions from an early stage in the student education process. This will make businesses an equal stakeholder in student education.
* Businesses and education institutions will be able to identify and nurture talent at global scale.
* As all the stakeholders of education, the government, policymakers, education institutions, staff, students and employers, are on the same platform it will help education institutions and policymakers to rapidly adapt to feedback from society and businesses.
* Government will have insight and some control over the cost of education because it will provide grants and or loans to education institutions rather than students.
* At present students are stuck with their first choice of course as they cannot afford to change course due to the cost of switching. After deploying this platform students will be able to easily change courses as there will be no cost for doing so.
* As loans will not be required for education, citizens will be easily encouraged to take up continuous learning to reduce unemployment.

The advantage of this system is that students are free of debt, can participate in continuous learning and can switch courses easily while studying. Education institutions and its staff will make better income using this platform in the long-term for their employment lifetime, students will get access to education for free, industry will get access to required talent, government will reduce its student loan burden and policymakers will get access to timely better data for decision making. This shall also make the education sector attractive for recruitment.

Other value added services can be developed by other non-profits on top of this platform:
* Education institution course catalogue
* Student CV listing
* Employer job listing
* Data analytics for policymakers
* Government education policy announcements
* Student career counselling
* Unemployment support system

#### Challenges for the platform
* This project will bring about a positive social change, and that will take time.
* Education institutions may oppose this as there is no immediate return on investment. This can be mitigated by the government providing loans/grants until the income of education institutions and its staff are self-sustainable.
* Affected private parties and lobby groups may oppose this platform.
* On the platform the association of a student to everyone involved in that student's education should ideally start from nursery/kindergarten.
* Students and staff travel across countries for study and employment hence the platform should be able to deduct and transfer payment across borders.
* In some countries income is never deposited in banks e.g. in India certain types of employees are mostly paid cash-in-hand rather than via bank account.
* Fear that humanities/arts/liberal-arts education will suffer due to this policy(this is already happening even without this platform[^34]). The right approach is to blend liberal arts courses with STEM courses to form STEAM courses[^32].

#### Appendix
###### Elastic payment system
![Elastic payment system](income_contribution_component.png "Elastic payment system")
* An education institution sets every year the base salary of a staff as it is done in the present system.
* Staff get paid the usual base salary(from the institution) which also includes payment from students.
* Once the payment from students is equal to or greater than the staff's base salary from the institution, then the staff does not get the salary from the institution/government and gets paid completely from student payments.
* If the payments from students go below the base salary of the staff, then the institution/government pays to fill the shortfall to reach the base salary threshold.

#### References
[^1]: https://www.youtube.com/embed/xlyBfInS7ec?start=39&end=63
[^2]: https://researchbriefings.parliament.uk/ResearchBriefing/Summary/CBP-8348
[^3]: https://londoneconomics.co.uk/wp-content/uploads/2017/07/LE-Impact-of-student-loan-repayments-on-graduate-taxation-FINAL.pdf  _(Page vi and vii, Table 1 and 2)_
[^4]: https://www.newyorkfed.org/research/staff_reports/sr733.html
[^5]: https://nces.ed.gov/programs/digest/d18/index.asp _(table 106.10)_
[^6]: https://studentaid.gov/sites/default/files/fsawg/datacenter/library/PortfolioSummary.xls
[^7]: https://www.newyorkfed.org/medialibrary/media/research/staff_reports/sr733.pdf _(Page 36 Figure 2)_
[^8]: https://www.cbo.gov/system/files?file=2019-05/51310-2019-05-studentloan.pdf _(Table 5)_
[^9]: https://www.brookings.edu/research/the-looming-student-loan-default-crisis-is-worse-than-we-thought
[^10]: https://ticas.org/our-work/student-debt
[^11]: https://www.federalreserve.gov/publications/files/consumer-community-context-201901.pdf#ConsumerCommunityContext_Issue1.indd%3A.4999%3A17 _(Page 2)_
[^12]: https://www.forbes.com/sites/robertfarrington/2019/04/12/income-sharing-agreements-to-pay-for-college
[^13]: https://nces.ed.gov/programs/coe/indicator_slc.asp
[^14]: https://learningpolicyinstitute.org/sites/default/files/product-files/A_Coming_Crisis_in_Teaching_REPORT.pdf _(Fig. 16 Page 39)_
[^15]: https://nces.ed.gov/programs/digest/d18/index.asp _(table 208.20)_
[^16]: https://nces.ed.gov/programs/digest/d18/index.asp
[^17]: https://researchbriefings.parliament.uk/ResearchBriefing/Summary/SN01079
[^18]: https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/805104/ANNEX_Estimating_the_changing_cost_of_HE.pdf _(Page 9)_
[^19]: https://www.ifs.org.uk/publications/14369
[^20]: https://www.bbc.co.uk/news/education-46180290
[^21]: https://publications.parliament.uk/pa/cm201719/cmselect/cmpubacc/1527/152703.htm#_idTextAnchor000
[^23]: https://edexec.co.uk/retention-rate-for-teachers-worsens
[^24]: https://www.nfer.ac.uk/news-events/nfer-blogs/latest-teacher-retention-statistics-paint-a-bleak-picture-for-teacher-supply-in-england
[^25]: https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/811622/SWFC_MainText.pdf _(Page 6)_
[^26]: https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/748164/Teachers_Analysis_Compendium_4_.pdf _(Page 66)_
[^27]: https://commonslibrary.parliament.uk/research-briefings/cbp-8577
[^28]: https://www.gov.uk/repaying-your-student-loan
[^29]: https://studentaid.gov/manage-loans/repayment
[^30]: https://www.ted.com/talks/dan_pallotta_the_way_we_think_about_charity_is_dead_wrong
[^31]: https://www.ted.com/talks/melinda_gates_what_nonprofits_can_learn_from_coca_cola
[^32]: https://www.amazon.co.uk/Arts-Schools-Principles-Practice-Provision/dp/0903319233
[^33]: https://studentaid.gov/manage-loans/default
[^34]: http://researchbriefings.files.parliament.uk/documents/SN06710/SN06710.pdf _(Page 15 Section 3)_
